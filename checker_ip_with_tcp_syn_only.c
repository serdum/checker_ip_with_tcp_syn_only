#include "checker_ip_with_tcp_syn_only.h"

int ports[] = {/*80, 81,*/ 1080, 443, 3128, 8080/*, 3180, 8080, 443, 3128 */};
const int ports_size = 4;
long bandwidth = 10000; //50*100*1000; // 1K  rate in bits per second
char *iface = "enp1s0";

char *source_ip ;
FILE *file, *file_ok;
int len_file = 0;
unsigned int all_ips = 0;

int stop_all = 0;
int starting_sniffer_syn = 0;
int stop_sniffer_syn = 0;

int checker_ip_with_tcp_syn_only()
{

    printf("checker_ip_with_tcp_syn_only\n");
    // http://natalia.appmat.ru/c&c++/lezione5.php - char

    char *file_name_ok = "checked_files.txt";
    if( access( file_name_ok, F_OK ) != -1 )
    {
        // file exists
        if (!(file_ok = fopen(file_name_ok, "r")))
        {
            printf("err open r %s",file_name_ok);
            return;
        }
    }
    else     // file doesn't exist
    {
        if (!(file_ok = fopen(file_name_ok, "w")))
        {
            printf("err open w %s",file_name_ok);
            return;
        }
    }
    fclose(file_ok);

    DIR* dir = NULL;
    struct dirent* ent = NULL;
    struct stat st;
    dir = opendir("range");
    if (dir == NULL)
    {
        printf("Error: failed to open directory '%s' (%d, %s)\n", ".",
               errno, strerror(errno));
        return;
    }

    ent = readdir(dir);

//
    while (ent != NULL)
    {
        char gg = NULL;
        if (!(file_ok = fopen(file_name_ok, "r"))) {
            printf("err open r %s",file_name_ok);
            return;
        }
        stat(ent->d_name, &st);
        if (S_ISDIR(st.st_mode)) {
            if (strstr(ent->d_name,"range_") != NULL) {
                char line[40];
                while (fgets(line, sizeof(line), file_ok) != NULL) {
                    gg = strstr(line, ent->d_name);
                    if (gg != NULL) {
                        printf("%s\n", ent->d_name);
                        break;
                    }
                }
                if (gg == NULL) {
                 fclose(file_ok);
                break;
                }
            }
        }
        fclose(file_ok);
        ent = readdir(dir);
    }

    if (ent == NULL)
    {
        closedir(dir);
        printf("end work\n");
        return;
    }

    char file_name = "range/" << ent->d_name;
    file = fopen(file_name, "r");
    if(file == NULL)
    {
        printf("не могу открыть файл '%s'","range.txt \n");
        return 0;
    }

    char line[40];
    while (fgets(line, sizeof(line), file) != NULL)
    {
        char *df = strchr(line, '-');
        if (df)
        {
            *df = '\0';
            unsigned int ipFrom = atoll(line);
            unsigned int ipTo = atoll(df + 1);
            all_ips += ipTo - ipFrom;
            ++len_file;
        }
    };
    printf("all ranges %u all ips %u\n",len_file, all_ips);
    close(file);

    file = fopen("range.txt","r");
    if(file == NULL)
    {
        printf("не могу открыть файл '%s'","range.txt \n");
        return 0;
    }

    struct in_addr default_ip;
    source_ip = malloc(INET_ADDRSTRLEN);

    // Get the source hardware address, and give it to the probe
    // module

    if (get_iface_hw_addr(iface, hw_mac))
    {
        printf("could not retrieve hardware address for "
               "interface: %s", iface);
        return -1;
    }
    printf("source MAC address %02x:%02x:%02x:%02x:%02x:%02x as hw "
           "interface for %s",
           hw_mac[0], hw_mac[1], hw_mac[2],
           hw_mac[3], hw_mac[4], hw_mac[5],
           iface);

    if (get_iface_ip(iface, &default_ip) < 0)
    {
        printf( "could not detect default IP address for %s."
                " Try specifying a source address (-S).", iface);
    }
    inet_ntop(AF_INET, &default_ip, source_ip, INET_ADDRSTRLEN);
    printf( "\nuse default address: %s", source_ip);

    struct in_addr gw_ip;
    memset(&gw_ip, 0, sizeof(struct in_addr));
    if (get_default_gw(&gw_ip, iface) < 0)
    {
        printf("could not detect default gateway address for %s."
               " Try setting default gateway mac address (-G).", iface);
    }
    printf("\nfound gateway IP %s on %s", inet_ntoa(gw_ip), iface);


    memset(&gw_mac, 0, MAC_ADDR_LEN_BYTES);
    if (get_hw_addr(&gw_ip, iface, gw_mac))
    {
        printf("could not detect GW MAC address for %s on %s."
               " Try setting default gateway mac address (-G), or run"
               " \"arp <gateway_ip>\" in terminal.",
               inet_ntoa(gw_ip), iface);
    }

    printf("\ngateway MAC address %02x:%02x:%02x:%02x:%02x:%02x",
           gw_mac[0], gw_mac[1], gw_mac[2],
           gw_mac[3], gw_mac[4], gw_mac[5]);


    int num_cores = sysconf(_SC_NPROCESSORS_ONLN);
    if (num_cores <= 0) num_cores = 1;
    pin_cores_len = (uint32_t) num_cores;
    pin_cores = calloc(pin_cores_len,
                       sizeof(uint32_t));
    int i;
    for (i = 0; i < pin_cores_len; ++i)
    {
        pin_cores[i] = i;
    }

    senders = num_cores;
    //senders = 2;
    ///////////////////////////////////////////////////////////////////

    tsend = malloc(senders * sizeof(pthread_t));

    printf("\n%d cores cpu \n", num_cores);


    stop_sniffer_syn = 0;
    pthread_t sniffer_syn;
    if( pthread_create( &sniffer_syn , NULL ,
                        (void *(*)(void *)) start_sniffer_syn , NULL) < 0 )
    {
        printf ("Could not create sniffer syn thread. Error number : %d . Error message : %s \n" , errno , strerror(errno));
        exit(0);
    }

    while (starting_sniffer_syn == 0) {};

    // тут начало цикла
    struct timespec ts = {0.500}; // 500 мс
    struct timespec rem;
    int r;
    while (1)
    {

        // start threads senders syn

        tsend = malloc(senders * sizeof(pthread_t));
        int cpu = 0;
        for ( i = 0; i < senders; i++)
        {

            send_arg_t *arg = malloc(sizeof(send_arg_t));

            arg->cpu = pin_cores[cpu % pin_cores_len];
            cpu += 1;

            int r = pthread_create(&tsend[i], NULL, start_send, arg);
            if (r != 0)
            {
                printf("unable to create send thread");
                exit(EXIT_FAILURE);
            }
        }

        // wait for completion
        for (i = 0; i < senders; i++)
        {
            int r = pthread_join(tsend[i], NULL);
            if (r != 0)
            {
                printf("unable to join send syn thread");
                exit(EXIT_FAILURE);
            }
        }


        if (stop_all == 1)
        {
            sleep(3);
            stop_sniffer_syn = 1;
            // wait stoping sender syn
            r = pthread_join(sniffer_syn, NULL);
            if (r != 0)
            {
                printf("unable to join sniffer_syn thread");
                exit(EXIT_FAILURE);
            }
            break;
        }
    } // while (1);



    close(file);

    return 0;


}

int get_iface_ip(char *iface, struct in_addr *ip)
{
    int sock;
    struct ifreq ifr;
    memset(&ifr, 0, sizeof(struct ifreq));
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
    {
        // std::cout << "get-iface-ip failure opening socket: %s"
        //   << std::endl;  //  << strerror(errno));
    }
    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name, iface, IFNAMSIZ-1);

    if (ioctl(sock, SIOCGIFADDR, &ifr) < 0)
    {
        close(sock);
        //    log_fatal("get-iface-ip", "ioctl failure: %s", strerror(errno));
    }
    ip->s_addr =  ((struct sockaddr_in*) &ifr.ifr_addr)->sin_addr.s_addr;
    close(sock);
    return EXIT_SUCCESS;
}

// gw and iface[IF_NAMESIZE] MUST be allocated
int _get_default_gw(struct in_addr *gw, char *iface)
{
    struct rtmsg req;
    unsigned int nl_len;
    char buf[8192];
    struct nlmsghdr *nlhdr;

    if (!gw || !iface)
    {
        return -1;
    }

    // Send RTM_GETROUTE request
    memset(&req, 0, sizeof(req));
    int sock = send_nl_req(RTM_GETROUTE, 0, &req, sizeof(req));

    // Read responses
    nl_len = read_nl_sock(sock, buf, sizeof(buf));
    if (nl_len <= 0)
    {
        return -1;
    }

    // Parse responses
    nlhdr = (struct nlmsghdr *)buf;
    while (NLMSG_OK(nlhdr, nl_len))
    {
        struct rtattr *rt_attr;
        struct rtmsg *rt_msg;
        int rt_len;
        int has_gw = 0;

        rt_msg = (struct rtmsg *) NLMSG_DATA(nlhdr);

        if ((rt_msg->rtm_family != AF_INET) || (rt_msg->rtm_table != RT_TABLE_MAIN))
        {
            return -1;
        }

        rt_attr = (struct rtattr *) RTM_RTA(rt_msg);
        rt_len = RTM_PAYLOAD(nlhdr);
        while (RTA_OK(rt_attr, rt_len))
        {
            switch (rt_attr->rta_type)
            {
            case RTA_OIF:
                if_indextoname(*(int *) RTA_DATA(rt_attr), iface);
                break;
            case RTA_GATEWAY:
                gw->s_addr = *(unsigned int *) RTA_DATA(rt_attr);
                has_gw = 1;
                break;
            }
            rt_attr = RTA_NEXT(rt_attr, rt_len);
        }

        if (has_gw)
        {
            return 0;
        }
        nlhdr = NLMSG_NEXT(nlhdr, nl_len);
    }
    return -1;
}

int get_default_gw(struct in_addr *gw, char *iface)
{
    char _iface[IF_NAMESIZE];
    memset(_iface, 0, IF_NAMESIZE);

    _get_default_gw(gw, _iface);
    if (strcmp(iface, _iface) != 0)
    {
        printf("interface specified (%s) does not match "
               "the interface of the default gateway (%s). You will need "
               "to manually specify the MAC address of your gateway.",
               iface, _iface);
    }
    return EXIT_SUCCESS;
}


int get_hw_addr(struct in_addr *gw_ip, char *iface, unsigned char *hw_mac)
{
    char buf[8192];
    memset(&buf, 0, 8192);
    struct ndmsg req;
    memset(&req, 0, sizeof(struct ndmsg));
    struct nlmsghdr *nlhdr;

    if (!gw_ip || !hw_mac)
    {
        return -1;
    }
    // Send RTM_GETNEIGH request
    req.ndm_family = AF_INET;
    req.ndm_ifindex = if_nametoindex(iface);
    req.ndm_state = NUD_REACHABLE;
    req.ndm_type = NDA_LLADDR;

    int sock = send_nl_req(RTM_GETNEIGH, 1, &req, sizeof(req));

    // Read responses
    unsigned nl_len = read_nl_sock(sock, buf, sizeof(buf));
    if (nl_len <= 0)
    {
        return -1;
    }
    // Parse responses
    nlhdr = (struct nlmsghdr *)buf;
    while (NLMSG_OK(nlhdr, nl_len))
    {
        struct rtattr *rt_attr;
        struct rtmsg *rt_msg;
        int rt_len;
        unsigned char mac[6];
        struct in_addr dst_ip;
        int correct_ip = 0;

        rt_msg = (struct rtmsg *) NLMSG_DATA(nlhdr);

        if ((rt_msg->rtm_family != AF_INET))
        {
            return -1;
        }

        rt_attr = (struct rtattr *) RTM_RTA(rt_msg);
        rt_len = RTM_PAYLOAD(nlhdr);
        while (RTA_OK(rt_attr, rt_len))
        {
            switch (rt_attr->rta_type)
            {
            case NDA_LLADDR:
                if (RTA_PAYLOAD(rt_attr) != IFHWADDRLEN)
                {
                    // could be using a VPN
                    printf("Unexpected hardware address length (%d).\n\n"
                           "    If you are using a VPN, supply the --vpn flag (and provide an"
                           " interface via -i)",
                           RTA_PAYLOAD(rt_attr));
                    exit(1);
                }
                memcpy(mac, RTA_DATA(rt_attr), IFHWADDRLEN);
                break;
            case NDA_DST:
                if (RTA_PAYLOAD(rt_attr) != sizeof(dst_ip))
                {
                    // could be using a VPN
                    printf("Unexpected IP address length (%d).\n"
                           "    If you are using a VPN, supply the --vpn flag"
                           " (and provide an interface via -i)",
                           RTA_PAYLOAD(rt_attr));
                    exit(1);
                }
                memcpy(&dst_ip, RTA_DATA(rt_attr), sizeof(dst_ip));
                if (memcmp(&dst_ip, gw_ip, sizeof(dst_ip)) == 0)
                {
                    correct_ip = 1;
                }
                break;
            }
            rt_attr = RTA_NEXT(rt_attr, rt_len);
        }
        if (correct_ip)
        {
            memcpy(hw_mac, mac, IFHWADDRLEN);
            return 0;
        }
        nlhdr = NLMSG_NEXT(nlhdr, nl_len);
    }
    return -1;
}

int read_nl_sock(int sock, char *buf, int buf_len)
{
    int msg_len = 0;
    char *pbuf = buf;
    do
    {
        int len = recv(sock, pbuf, buf_len - msg_len, 0);
        if (len <= 0)
        {
            printf("recv failed: %s", strerror(errno));
            return -1;
        }
        struct nlmsghdr *nlhdr = (struct nlmsghdr *)pbuf;
        if (NLMSG_OK(nlhdr, ((unsigned int)len)) == 0 ||
                nlhdr->nlmsg_type == NLMSG_ERROR)
        {
            printf( "recv failed: %s", strerror(errno));
            return -1;
        }
        if (nlhdr->nlmsg_type == NLMSG_DONE)
        {
            break;
        }
        else
        {
            msg_len += len;
            pbuf += len;
        }
        if ((nlhdr->nlmsg_flags & NLM_F_MULTI) == 0)
        {
            break;
        }
    }
    while (1);
    return msg_len;
}

int send_nl_req(uint16_t msg_type, uint32_t seq,
                void *payload, uint32_t payload_len)
{
    int sock = socket(PF_NETLINK, SOCK_DGRAM, NETLINK_ROUTE);
    if (sock < 0)
    {
        printf( "unable to get socket: %s", strerror(errno));
        return -1;
    }
    if (NLMSG_SPACE(payload_len) < payload_len)
    {
        close(sock);
        // Integer overflow
        return -1;
    }
    struct nlmsghdr *nlmsg;
    nlmsg = malloc(NLMSG_SPACE(payload_len));

    memset(nlmsg, 0, NLMSG_SPACE(payload_len));
    memcpy(NLMSG_DATA(nlmsg), payload, payload_len);
    nlmsg->nlmsg_type = msg_type;
    nlmsg->nlmsg_len = NLMSG_LENGTH(payload_len);
    nlmsg->nlmsg_flags = NLM_F_DUMP | NLM_F_REQUEST;
    nlmsg->nlmsg_seq = seq;
    nlmsg->nlmsg_pid = getpid();

    if (send(sock, nlmsg, nlmsg->nlmsg_len, 0) < 0)
    {
        printf( "failure sending: %s", strerror(errno));
        return -1;
    }
    free(nlmsg);
    return sock;
}

int get_iface_hw_addr(char *iface, unsigned char *hw_mac)
{
    int s;
    struct ifreq buffer;

    // Load the hwaddr from a dummy socket
    s = socket(PF_INET, SOCK_DGRAM, 0);
    if (s < 0)
    {
        printf("Unable to open socket: %s",
               strerror(errno));
        return EXIT_FAILURE;
    }
    memset(&buffer, 0, sizeof(buffer));
    strncpy(buffer.ifr_name, iface, IFNAMSIZ);
    ioctl(s, SIOCGIFHWADDR, &buffer);
    close(s);
    memcpy(hw_mac, buffer.ifr_hwaddr.sa_data, 6);
    return EXIT_SUCCESS;
}


#include <sched.h>

static void* start_send(void *arg)
{
    send_arg_t *s = (send_arg_t *) arg;
    //  printf("\nPinning a send thread to core %u", s->cpu);

    int core = s->cpu;
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(core, &cpuset);

    if (pthread_setaffinity_np(pthread_self(),
                               sizeof(cpu_set_t), &cpuset) != 0)
    {
        return 1;
    }
    send_run(core,s->work);
    free(s);
    return NULL;
}

// one sender thread
int send_run(int core_, char *work_)
{


    // sleep(44477);
    pthread_mutex_lock(&send_mutex);

    int core = core_;
    char *work = work_;

    printf("send thread started %d core\n",core);

    int sock = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    if (sock <= 0)
    {
        printf("\ncouldn't create socket. "
               "Are you root? Error: %s\n", strerror(errno));
    }

//   pthread_mutex_lock(&send_mutex);
    // Allocate a buffer to hold the outgoing packet
    char buf[MAX_PACKET_SIZE];
    memset(buf, 0, MAX_PACKET_SIZE);

    struct ifreq if_idx;
    memset(&if_idx, 0, sizeof(struct ifreq));
    if (strlen(iface) >= IFNAMSIZ)
    {
        printf("device interface name (%s) too long\n",
               iface);
        return EXIT_FAILURE;
    }
    strncpy(if_idx.ifr_name, iface, IFNAMSIZ-1);
    if (ioctl(sock, SIOCGIFINDEX, &if_idx) < 0)
    {
        perror("SIOCGIFINDEX");
        return EXIT_FAILURE;
    }

    int ifindex = if_idx.ifr_ifindex;
    // find source IP address associated with the dev from which we're sending.
    // while we won't use this address for sending packets, we need the address
    // to set certain socket options and it's easiest to just use the primary
    // address the OS believes is associated.
    struct ifreq if_ip;
    memset(&if_ip, 0, sizeof(struct ifreq));
    strncpy(if_ip.ifr_name, iface, IFNAMSIZ-1);
    if (ioctl(sock, SIOCGIFADDR, &if_ip) < 0)
    {
        perror("SIOCGIFADDR");
        return EXIT_FAILURE;
    }
    // destination address for the socket
    memset((void*) &sockaddr, 0, sizeof(struct sockaddr_ll));
    sockaddr.sll_ifindex = ifindex;
    sockaddr.sll_halen = ETH_ALEN;
    memcpy(sockaddr.sll_addr, gw_mac, ETH_ALEN);

    // MAC address length in characters
    char mac_buf[(ETHER_ADDR_LEN * 2) + (ETHER_ADDR_LEN - 1) + 1];
    char *p = mac_buf;
    int i;
    for(i=0; i < ETHER_ADDR_LEN; i++)
    {
        if (i == ETHER_ADDR_LEN-1)
        {
            snprintf(p, 3, "%.2x", hw_mac[i]);
            p += 2;
        }
        else
        {
            snprintf(p, 4, "%.2x:", hw_mac[i]);
            p += 3;
        }
    }
    // printf("\nsource MAC address %s", mac_buf);

    pthread_mutex_unlock(&send_mutex);

    int rate = 0;// rate in packets per second that the sender will maintain

    // concert specified bandwidth to packet rate
    if (bandwidth > 0)
    {
        size_t pkt_len = 54;
        pkt_len *= 8;
        pkt_len += 8*24; // 7 byte MAC preamble, 1 byte Start frame,
        // 4 byte CRC, 12 byte inter-frame gap
        if (pkt_len < 84*8)
        {
            pkt_len = 84*8;
        }
        // rate is a uint32_t so, don't overflow
        if (bandwidth / pkt_len > 0xFFFFFFFFu)
        {
            rate = 0;
        }
        else
        {
            rate = bandwidth / pkt_len;
            if (rate == 0)
            {
                printf("bandwidth %lu bit/s is slower than 1 pkt/s, "
                       "setting rate to 1 pkt/s", bandwidth);
                rate = 1;
            }
        }
        printf("using bandwidth %lu bits/s, rate set to %d pkt/s \n",
               bandwidth, rate);
    }

    // adaptive timing to hit target rate
    uint32_t count = 0;
    uint32_t last_count = count;
    double last_time = now();
    uint32_t delay = 0;
    int interval = 0;
//   uint32_t max_targets = 1; //s->state.max_targets;
    volatile int vi;
    struct timespec ts, rem;

    double send_rate = (double) rate / senders;
    double slow_rate = 50; // packets per seconds per thread
    // at which it uses the slow methos
    long nsec_per_sec = 1000 * 1000 * 1000;
    long long sleep_time = nsec_per_sec;
    if (rate > 0)
    {
        delay = 10000;

        if (send_rate < slow_rate)
        {
            // set the inital time difference
            sleep_time = nsec_per_sec / send_rate;
            last_time = now() - (1.0 / send_rate);
        }
        else
        {
            // estimate initial rate
            for (vi = delay; vi--; );
            delay *= 1 / (now() - last_time) / (rate / senders);
            interval = (rate / senders) / 20;
            last_time = now();
        }
    }


    // uint32_t curr = inet_addr("193.200.173.2");  // shard_get_cur_ip(s);
    int attempts = 1; //zconf.num_retries + 1;
    uint32_t idx = 0;
    while (1)
    {

        pthread_mutex_lock(&send_mutex);

        /*  initscr();
          timeout(1);
                 while ( getch() != ERR ){
              stop_all = 1;
                  break;

          }
          endwin();
          if (stop_all == 1)
              break;*/

        unsigned int ipFrom, ipTo;

        char line[40];

        if (fgets(line, sizeof(line), file) != NULL)
        {
            //  printf(line);
            //  printf("\n");
            char *df = strchr(line, '-');
            if (df)
            {
                *df = '\0';
                ipFrom = atoll(line);
                ipTo = atoll(df + 1);

                unsigned int al = ipTo - ipFrom;

                printf("tcp syn ipFrom = %u, ipTo %u  all %u  %u\n",ipFrom,ipTo, al, len_file);
                --len_file;

            }
        }
        else
        {
            stop_all = 1;
            pthread_mutex_unlock(&send_mutex);
            break;
        }

        pthread_mutex_unlock(&send_mutex);


        int ggg = 0;

        long int ii = ipFrom;
        for (ii; ii <= ipTo; ii++)   // диапазон или один адрес
        {
            if (ggg == 5) break;
            // ++ggg;

            uint32_t curr;


            long a_, b_, c_, d_, ip_, pp_;

            ip_ = ipFrom;
            ++ipFrom;
            pp_ = 256*256*256;
            a_ = ip_ / pp_;
            ip_ = ip_ - a_ * pp_;

            pp_ = 256*256;
            b_ = ip_ / pp_;
            ip_ = ip_ - b_ * pp_;

            pp_ = 256;
            c_ = ip_ / pp_;
            ip_ = ip_ - c_ * pp_;

            d_ = ip_;

            if (d_ == 0 || d_ == 255) continue;

            curr =
                d_ * 256*256*256 +
                c_ * 256*256 +
                b_ * 256 +
                a_;

            int  src_port = 3000 + core;
            uint32_t src_ip = inet_addr(source_ip);

            int length = 54;   // для TCP SYN  zconf.probe_module->packet_length;
            // int send_ip_pkts = 1;// я
            int any_sends_successful = 0;

            int port;
            int size_ports;
            size_ports = ports_size;

            for (port = 0; port < size_ports; ++port)
            {
                // adaptive timing delay
                if (delay > 0)
                {
                    count++;
                    if (send_rate < slow_rate)
                    {
                        double t = now();
                        double last_rate = (1.0 / (t - last_time));
                        sleep_time *= ((last_rate / send_rate) + 1) / 2;
                        ts.tv_sec = sleep_time / nsec_per_sec;
                        ts.tv_nsec = sleep_time % nsec_per_sec;
                        //  printf("\nsleep for %d sec, %ld nanoseconds",ts.tv_sec, ts.tv_nsec);
                        while (nanosleep(&ts, &rem) == -1) {}
                        last_time = t;
                    }
                    else
                    {
                        ts.tv_sec = delay / nsec_per_sec;
                        ts.tv_nsec = delay % nsec_per_sec;
                        // printf("\nsleep for %d sec, %ld nanoseconds",ts.tv_sec, ts.tv_nsec);
                        while (nanosleep(&ts, &rem) == -1) {}
                        //for (vi = delay; vi--; );
                        if (!interval || (count % interval == 0))
                        {
                            double t = now();
                            delay *= (double)(count - last_count)
                                     / (t - last_time) / (rate / senders);
                            if (delay < 1)
                                delay = 1;
                            last_count = count;
                            last_time = t;
                        }
                    }
                }
                port_h_t  target_port = ports[port];
                syn_init_perthread(buf, hw_mac, gw_mac, target_port);
                syn_make_packet(buf, src_ip, curr, src_port);


                int rc =   sendto(sock, buf, length, 0,
                                  (struct sockaddr *) &sockaddr,
                                  sizeof(struct sockaddr_ll));

                if (rc < 0)
                {
                    struct in_addr addr;
                    addr.s_addr = curr;
                    char addr_str_buf[INET_ADDRSTRLEN];
                    const char *addr_str = inet_ntop(AF_INET, &addr, addr_str_buf, INET_ADDRSTRLEN);
                    if (addr_str != NULL)
                    {
                        printf("send_packet failed for %s. %s",
                               addr_str, strerror(errno));
                    }
                }
                else
                {
                    any_sends_successful = 1;
                    //break;
                }
            } // for (i1 = 0; i1 < attempts; ++i1) по портам

        }


    } // while (1)

    close(sock);

    printf("send thread finished core %d\n",core);

    return EXIT_SUCCESS;
}

double now(void)
{
    struct timeval now;
    gettimeofday(&now, NULL);
    return (double)now.tv_sec + (double)now.tv_usec/1000000.;
}

int syn_make_packet(void *buf, ipaddr_n_t src_ip, ipaddr_n_t dst_ip, int src_port)
{
    struct ether_header *eth_header = (struct ether_header *)buf;
    struct ip *ip_header = (struct ip*)(&eth_header[1]);
    struct tcphdr *tcp_header = (struct tcphdr*)(&ip_header[1]);
    uint32_t tcp_seq = 0;

    ip_header->ip_src.s_addr = src_ip;
    ip_header->ip_dst.s_addr = dst_ip;

    tcp_header->th_sport = htons(src_port); // htons(get_src_port(num_ports, probe_num, validation));
    tcp_header->th_seq = tcp_seq;
    tcp_header->th_sum = 0;
    tcp_header->th_sum = tcp_checksum(sizeof(struct tcphdr),
                                      ip_header->ip_src.s_addr, ip_header->ip_dst.s_addr, tcp_header);

    ip_header->ip_sum = 0;
    ip_header->ip_sum = zmap_ip_checksum((unsigned short *) ip_header);
}

int syn_init_perthread(void* buf, macaddr_t *src, macaddr_t *gw, port_h_t dst_port)
{
    memset(buf, 0, MAX_PACKET_SIZE);
    struct ether_header *eth_header = (struct ether_header *) buf;
    make_eth_header(eth_header, src, gw);
    struct ip *ip_header = (struct ip*)(&eth_header[1]);
    uint16_t len = htons(sizeof(struct ip) + sizeof(struct tcphdr));
    make_ip_header(ip_header, IPPROTO_TCP, len);
    struct tcphdr *tcp_header = (struct tcphdr*)(&ip_header[1]);
    make_tcp_header(tcp_header, dst_port);
    return EXIT_SUCCESS;
}

int icmp_init_perthread(void* buf, macaddr_t *src, macaddr_t *gw)
{
    memset(buf, 0, MAX_PACKET_SIZE);

    struct ether_header *eth_header = (struct ether_header *) buf;
    make_eth_header(eth_header, src, gw);

    struct ip *ip_header = (struct ip *) (&eth_header[1]);
    uint16_t len = htons(sizeof(struct ip) + sizeof(struct icmp) - 8);
    make_ip_header(ip_header, IPPROTO_ICMP, len);

    struct icmp *icmp_header = (struct icmp*)(&ip_header[1]);
    make_icmp_header(icmp_header);

    return EXIT_SUCCESS;
}

int icmp_make_packet(void *buf, ipaddr_n_t src_ip, ipaddr_n_t dst_ip)
{
    struct ether_header *eth_header = (struct ether_header *) buf;
    struct ip *ip_header = (struct ip *)(&eth_header[1]);
    struct icmp *icmp_header = (struct icmp*)(&ip_header[1]);
    uint16_t icmp_idnum = 12345; // validation[2] & 0xFFFF;

    ip_header->ip_src.s_addr = src_ip;
    ip_header->ip_dst.s_addr = dst_ip;

    icmp_header->icmp_id = icmp_idnum;
    icmp_header->icmp_cksum = 0;
    icmp_header->icmp_cksum = icmp_checksum((unsigned short *) icmp_header);

    ip_header->ip_sum = 0;
    ip_header->ip_sum = zmap_ip_checksum((unsigned short *) ip_header);

    return EXIT_SUCCESS;
}

int start_sniffer_syn(void * arg)
{

// https://www.opennet.ru/base/dev/traffic_analyze.txt.html
// SIOCGIFADDR  recvfrom
// https://gist.github.com/austinmarton/2862515



    printf("started start_sniffer_syn\n") ;

    FILE *file_ip_port;
    char file_name[128];
    int ii = 0;
    sprintf(file_name,"ip_port_%d.txt",ii);

    while (access(file_name,0) != -1)
    {

        file_ip_port = fopen(file_name,"r");
        if (file_ip_port)
        {
            fseek(file_ip_port,0,SEEK_END);
            long size = ftell(file_ip_port);
            if (size == 0)
            {
                close(file_ip_port);
                remove(file_name);
            }
        }
        ii++;
        sprintf(file_name,"ip_port_%d.txt",ii);
    }


    ii = 0;
    sprintf(file_name,"ip_port_%d.txt",ii);

    while (access(file_name,0) != -1)
    {
        ii++;
        sprintf(file_name,"ip_port_%d.txt",ii);
    };

    file_ip_port = fopen(file_name,"wb");
    if (file_ip_port == NULL)
    {
        printf("не могу открыть файл '%s'","ip_for_syn \n");
        //return 0;
    }

    char ip_from[INET6_ADDRSTRLEN];
    int sock, ret, i;
    int sockopt;
    ssize_t bytes;
    struct ifreq ifopts;	// set promiscuous mode
    struct ifreq if_ip;	// get ip addr
    struct sockaddr_storage their_addr;
    uint8_t buf[65536];

    // Header structures
    struct ether_header *eth = (struct ether_header *) buf;
    struct iphdr *iph = (struct iphdr *) (buf + sizeof(struct ether_header));
//    struct udphdr *udph = (struct udphdr *) (buf + sizeof(struct iphdr) + sizeof(struct ether_header));
    struct tcphdr *tcph = (struct tcphdr *)
                          (buf + sizeof(struct iphdr) + sizeof(struct ether_header));
    memset(&if_ip, 0, sizeof(struct ifreq));

    /* Open PF_PACKET socket, listening for EtherType  = 0x0800 */
    if ((sock = socket(PF_PACKET, SOCK_RAW, htons(0x0800))) == -1)
    {
        perror("listener: socket");
        return -1;
    }

    /* Set interface to promiscuous mode - do we need to do this every time? */
    strncpy(ifopts.ifr_name, iface, IFNAMSIZ-1);
    ioctl(sock, SIOCGIFFLAGS, &ifopts);
    ifopts.ifr_flags |= IFF_PROMISC;
    ioctl(sock, SIOCSIFFLAGS, &ifopts);

    // Allow the socket to be reused - incase connection is closed prematurely
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &sockopt, sizeof sockopt) == -1)
    {
        perror("setsockopt");
        close(sock);
        exit(EXIT_FAILURE);
    }

    // Bind to device
    if (setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE, iface, IFNAMSIZ-1) == -1)
    {
        perror("SO_BINDTODEVICE");
        close(sock);
        exit(EXIT_FAILURE);
    }

    starting_sniffer_syn = 1;

    while (1)
    {
        if (stop_sniffer_syn) break;

        bytes = recvfrom(sock, buf, sizeof(buf), 0, NULL, NULL);

        // Check the packet is for me
        if (eth->ether_dhost[0] == hw_mac[0] &&
                eth->ether_dhost[1] == hw_mac[1] &&
                eth->ether_dhost[2] == hw_mac[2] &&
                eth->ether_dhost[3] == hw_mac[3] &&
                eth->ether_dhost[4] == hw_mac[4] &&
                eth->ether_dhost[5] == hw_mac[5])
        {}
        else continue;

        // Get source IP
        ((struct sockaddr_in *)&their_addr)->sin_addr.s_addr = iph->saddr;
        inet_ntop(AF_INET, &((struct sockaddr_in*)&their_addr)->sin_addr, ip_from, sizeof ip_from);

        /* Look up my device IP addr if possible */
        strncpy(if_ip.ifr_name, iface, IFNAMSIZ-1);
        if (ioctl(sock, SIOCGIFADDR, &if_ip) >= 0)   /* if we can't check then don't */
        {
            //  printf("Source IP: %s My IP: %s\n", sender,
            //         inet_ntoa(((struct sockaddr_in *)&if_ip.ifr_addr)->sin_addr));
            /* ignore if I sent it */
            if (strcmp(ip_from, inet_ntoa(((struct sockaddr_in *)&if_ip.ifr_addr)->sin_addr)) == 0)
            {
                printf("but I sent it :(\n");
                continue;
            }


            if(iph->protocol == 6 && iph->version == 4)
            {
                if  (tcph->th_flags == 18)
                {

                    int port_from =  ntohs(tcph->th_sport);
                    //   printf("ip: %s port: %d\n", sender,  port_from);
                    //inet_ntoa(((struct sockaddr_in *)&if_ip.ifr_addr)->sin_addr));


                    int port_to =  ntohs(tcph->th_dport);
                    fprintf(file_ip_port, "%s:%d\n", ip_from, port_from);
                    fflush(file_ip_port);


                }
            }
        }

    }

    close(sock);
    fclose(file_ip_port);
    printf("Sniffer syn finished.\n");
}

