TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.c \
    packet.c \
    checker_ip_with_tcp_syn_only.c

TARGET = checker_ip_with_tcp_syn_only

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    packet.h \
    includes.h \
    checker_ip_with_tcp_syn_only.h

LIBS +=  -lpthread -lncursesw

