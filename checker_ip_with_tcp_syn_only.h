#ifndef CHECKER_IP_WITH_TCP_SYN_ONLY_H
#define CHECKER_IP_WITH_TCP_SYN_ONLY_H
#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <sched.h>
#include <errno.h>
#include <pwd.h>
#include <time.h>
#include <sys/stat.h>
#include <dirent.h>


#include <sys/ioctl.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <linux/if_ether.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netinet/udp.h>
#define __FAVOR_BSD           // Use BSD format of tcp header
#include <netinet/tcp.h>
#include <net/ethernet.h>

#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/ioctl.h>
#include <netpacket/packet.h>
#include <pthread.h>
#include <packet.h>

#include <ncurses.h>





typedef uint32_t ipaddr_n_t; // IPv4 address network order
typedef uint32_t ipaddr_h_t; // IPv4 address host order
typedef uint16_t port_n_t; // port network order
typedef uint16_t port_h_t; // port host order
typedef unsigned char macaddr_t;



// Dummy sockaddr for sendto
static struct sockaddr_ll sockaddr;

#define MAC_ADDR_LEN ETHER_ADDR_LEN
#define MAC_ADDR_LEN_BYTES 6
#define MAX_PACKET_SIZE 4096
#define UNUSED __attribute__((unused))

typedef unsigned char macaddr_t;
macaddr_t hw_mac[MAC_ADDR_LEN_BYTES];
macaddr_t gw_mac[MAC_ADDR_LEN_BYTES];


// Lock for send run
static pthread_mutex_t send_mutex = PTHREAD_MUTEX_INITIALIZER;

int senders;
int pin_cores_len;
int *pin_cores;
pthread_t *tsend;

typedef struct send_arg
{
    uint32_t cpu;
    int sock;
    char *work;
} send_arg_t;

typedef struct arg_for_icmp
{
    FILE *file_in;
    short int sleep_;
    short int end_;
} arg_for_icmp_t;


int get_iface_hw_addr(char *iface, unsigned char *hw_mac);
int get_iface_ip(char *iface, struct in_addr *ip);
int get_default_gw(struct in_addr *gw, char *iface);
int _get_default_gw(struct in_addr *gw, char *iface);
int get_hw_addr(struct in_addr *gw_ip, char *iface, unsigned char *hw_mac);
int send_nl_req(uint16_t msg_type, uint32_t seq,
                void *payload, uint32_t payload_len);
int read_nl_sock(int sock, char *buf, int buf_len);
static void* start_send(void *arg);
int send_run(int core_, char *work_);
double now(void);
int syn_make_packet(void *buf, ipaddr_n_t src_ip, ipaddr_n_t dst_ip, int src_port);
int syn_init_perthread(void* buf, macaddr_t *src, macaddr_t *gw, port_h_t dst_port);
int icmp_init_perthread(void* buf, macaddr_t *src, macaddr_t *gw);
int icmp_make_packet(void *buf, ipaddr_n_t src_ip, ipaddr_n_t dst_ip);
int start_sniffer_syn(void * arg);

#endif // CHECKER_IP_WITH_TCP_SYN_ONLY_H
